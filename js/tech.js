$(function() {
	$('.block-holder').each(function () {
		var $context = $(this);
		var $slider = $('.block-holder__scale', $context);
		var $container = $('.block-holder__container', $context);
		var $sliderInput = $slider.find('input');
		var $sliderHandleVal = $('<span class="handle-val"></span>');
		var $edgeLink = $('<a href="#" class="tech-holder__highlight">Края</a>');

		var width = $context.data('width') || $context.find('.content-center').width();
		var maxWidth = $(window).width();
		var minWidth = $context.data('min') || 50;
		var sliderStep = $context.data('step') || 1;

		function changeSliderVal (val) {
			$sliderInput.val(val);
		}

		function changeContextWidth (val) {
			var $targets = $context.find('.bem');

			$container.width(val);
			$targets.trigger('resize.block');
		}

		function changeHandleVal (val) {
			$sliderHandleVal.text(val);
		}

		$slider
			.slider({
				min: minWidth,
				max: maxWidth,
				step: sliderStep,
				value: width,
				range: 'min',

				slide: function (event, ui) {
					changeSliderVal(ui.value);
					changeHandleVal(ui.value);
					changeContextWidth(ui.value);
				}
			})
			.find('.ui-slider-handle')
			.append($sliderHandleVal);



		$edgeLink.on('click', function () {
			$context.toggleClass('_highlighted');
			return false;
		});

		changeContextWidth(width);
		changeHandleVal(width);
		$sliderInput.val(width);
		$sliderInput.on('change', function() {
			var val = parseFloat($(this).val());
			$slider.slider('value', val);
			changeContextWidth(val);
			changeHandleVal(val);
		});
	})
});

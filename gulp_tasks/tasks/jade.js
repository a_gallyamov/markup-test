var	gulp = require('gulp');
var jade = require('gulp-jade');
var plumber = require('gulp-plumber');
var	watcher = require('gulp-watch');
var config = require('../config');
var browserSync = require('./browserSync');


var isFast = config.env.isFast;
var isLocal = config.env.isLocal;
var isProduction = config.env.isProduction;
var paths = isLocal ? config.localPaths : config.paths;
var watchPage = config.env.watchPage;


gulp.task('jade', function() {
	if(isProduction) { return; }
	if(isFast) {
		if (watchPage) {
			gulp.start('jade:page', ['concat:jade']);
		} else {
			gulp.src([paths.pages + '*.jade'])
				.pipe(watcher([paths.pages + '*.jade']))
				.pipe(plumber())
				.pipe(jade({
					pretty: true
				}))
				.pipe(gulp.dest(paths.base));
		}
	} else {
		return gulp.src([paths.pages + '*.jade'])
			.pipe(plumber())
			.pipe(jade({
				pretty: true
			}))
			.pipe(gulp.dest(paths.base));
	}
});

gulp.task('jade:page', function() {
	return gulp.src([paths.pages + watchPage + '.jade'])
		.pipe(plumber())
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest(paths.base));
});
